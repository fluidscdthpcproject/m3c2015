"""An illustrative example for integrating an ODE
Uses scipy's odeint function to solve dy/dt = -ay
After solving the ODE, the numerical and exact solutions
are compared and plotted
"""

#import modules
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

#function provides RHS to odeint
def RHS(y,t,a=1.0):
    return -a*y
    

#timepsan
t=np.linspace(0,1,1000)

#initial condition
y0 = 1.0

#set parameter, a
a = 1.5

#integrate ODE specified by RHS
Y = odeint(RHS,y0,t,args=(a,))

#exact solution and error
Yexact = y0*np.exp(-a*t)
error = np.abs(Yexact-Y.T) #the .T takes the transpose of Y
print "max error=",error.max()

#make figure showing solutions
plt.figure()
plt.plot(t,Y)
plt.plot(t,Yexact,'r--')
plt.legend(['computed','exact'])
plt.xlabel('t')
plt.ylabel('y')
plt.title('solution to dy/dt = -ay')
plt.show()