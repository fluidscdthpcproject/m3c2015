"""Python module to compute
sqrt function with Newton's method"""

def sqrt2(a,debug=False):
    """function to compute sqrt(a) with
    Newton's method"""

    #check input
    assert type(a) is int or type(a) is float, "error, input must be a number"
    assert a>=0, "error, input must be non-negative"


    x0 = 1.0 #initial guess for sqrt(a)

    imax = 10000 #maximum number of iterations
    tol = 1.0e-14 #convergence criteria
    
    #loop implementing Newton's method
    for i in range(imax):
        x1 = a/(2*x0) + x0/2
        deltax = abs(x0-x1)
        if debug:
            print "after iteration %d, delta x= %16.12f" %(i+1,deltax)
        if deltax<tol:
            if debug:
                print "converged"
            break
        x0 = x1
    
    return x1    


def test():
    """test sqrt2 for a few values"""
    from math import sqrt
    avalues = [0.1, 5.0, 37.0, 163.9]
    tol = 1.0e-12
    for a in avalues:
        s = sqrt(a)
        s2 = sqrt2(a)
        error = abs(s-s2)
        print "test sqrt with a=%6.4f, error=%20.16f" %(a,error)
        assert error<tol, "error, sqrt2 failed for a=%6.4f"%(a)
    

if __name__ == "__main__":
    test()   


"""Note: There are three ways to 'run' the code. One is to import the module in the
ipython terminal and call the individual functions. You can also use "run mysqrt"
in the ipython terminal or "python mysqrt.py" in the general unix terminal. In these
latter two cases, __name__ == "__main__" and the if statement above is true and the
test function is called. Flags can be used with the run command to obtain timming and
profiling information: run -t, run -p
"timeit" can be used in the ipython terminal to time any available function 
"""
